package gnirps;

import java.time.Clock;

public class Chronometer {
	private final Clock clock;
	private long startTimeMillis = 0L;

	public Chronometer(){
		this.clock = Clock.systemDefaultZone();
	}

	public Chronometer(Clock clock) {
		this.clock = clock;
	}

	public void start() {
		startTimeMillis = clock.millis();
	}

	public long elapsed() {
		return clock.millis() - startTimeMillis;
	}
}
