package gnirps;

import java.util.logging.Logger;

public class TimedInvocation {

	private static final Logger LOG = Logger.getLogger(TimedInvocation.class.getName());

	public <T> T invoke(InvocationPoint<T> invocationPoint) throws Exception {
		Chronometer watch = new Chronometer();
		watch.start();
		LOG.info("Start calculation");
		T result = invocationPoint.invoke();
		LOG.info(String.format("End calculation (%d ms)", watch.elapsed()));
		return result;
	}
}
