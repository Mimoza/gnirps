package gnirps;

import gnirps.annotation.Injectable;
import gnirps.scanner.ClassesScanner;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public final class Gnirps {
	private static final Logger LOG = Logger.getLogger(Gnirps.class.getName());
	private static final List<Class<?>> LISTE_CLAZZ = new ArrayList<>();
	private static final Map<String, Object> MAP_INJECTABLE = new HashMap<>();
	private static final Gnirps INSTANCE = new Gnirps();

	private Gnirps(){}
	public static Gnirps getConteneur(){
		return INSTANCE;
	}

	public static void demarre(Class<?> clazz){
		LOG.info("Démarrage du conteneur léger Gnirps");
		try {
			LISTE_CLAZZ.addAll(ClassesScanner.getClasses(clazz.getPackage().getName()));
		} catch (ClassNotFoundException | IOException e) {
			LOG.severe("Erreur lors du chargement des classes");
			e.printStackTrace();
		}
		repereInjectable();
		inject();
		LOG.info("Fin du démarrage de Gnirps");
	}

	private static void inject(){
		for(Class<?> clazz : LISTE_CLAZZ){
			clazz.getDeclaredFields();
		}
	}

	private static void repereInjectable(){
		for(Class<?> clazz : LISTE_CLAZZ){
			if(Stream.of(clazz.getAnnotations()).anyMatch(annotation -> annotation instanceof Injectable)) {
				ajouteInjectable(clazz);
			}
		}
	}

	private static void ajouteInjectable(Class<?> clazz){
		Class<?>[] interfaces = clazz.getInterfaces();
		for (Class<?> uneInterface : interfaces) {
			LOG.info("Ajout de la class " + clazz.getName() + " en tant qu'objet injectable pour l'" + uneInterface);
			Object ancienneValeur = MAP_INJECTABLE.put(uneInterface.getName(), creerInstance(clazz));
			if ( ancienneValeur != null){
				LOG.severe(String.format("Attention remplacement de %s par %s pour %s", ancienneValeur.getClass().getName(), clazz.getName(), uneInterface.getName()));
			}
		}
		LOG.info("Ajout de la class " + clazz.getName() + " en tant qu'objet injectable pour elle même");
		MAP_INJECTABLE.put(clazz.getName(), clazz);
	}

	//https://www.developpez.net/forums/d1405993/java/general-java/instanciation-dynamique-d-objets/#post7639423
	public static Object getImplementation(String nomClass) {
		Object instance = null;
		try {
			Class<?> clazz = Class.forName(nomClass);
			instance = creerInstance(clazz);
		} catch (ClassNotFoundException cnfe) {
			LOG.log(Level.SEVERE, "La classe " + nomClass + " n'existe pas", cnfe);
		}
		return instance;
	}

	private static Object creerInstance(Class<?> clazz){
		Object instance= null;
		try {
			instance = clazz.getConstructor().newInstance();
		} catch (InstantiationException ie) {
			LOG.log(Level.SEVERE, "La classe " + clazz.getName() + " n'est pas instanciable", ie);
		} catch (IllegalAccessException iae) {
			LOG.log(Level.SEVERE, "La classe " + clazz.getName() + " n'est pas accessible", iae);
		} catch (NoSuchMethodException nsme) {
			LOG.log(Level.SEVERE, "La méthode d'instanciation de la class " + clazz.getName() + " n'existe pas", nsme);
		} catch (InvocationTargetException ite) {
			LOG.log(Level.SEVERE, "L'invocation du constructeur de la class " + clazz.getName() + " n'est pas accessible", ite);
		}
		return instance;
	}

	public static <O> O getInstance(Class<?> clazz){
		return (O)MAP_INJECTABLE.get(clazz.getName());
	}
}
