package gnirps;

@FunctionalInterface
public interface InvocationPoint<T> {

	T invoke() throws Exception;
}
