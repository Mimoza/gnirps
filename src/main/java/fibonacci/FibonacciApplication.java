package fibonacci;

import fibonacci.calculator.FibonacciCalculator;
import fibonacci.calculator.impl.FibonacciCalculatorImpl;
import fibonacci.calculator.impl.FibonacciCalculatorImpl2;
import gnirps.Gnirps;
import gnirps.proxy.Proxifier;
import gnirps.proxy.TimedInvocationHandler;

import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Logger;

import static java.lang.String.format;

//https://blog.zenika.com/2016/11/18/implementer-un-mecanisme-daop-sans-framework/
public final class FibonacciApplication {

	private static final Logger LOG = Logger.getLogger(FibonacciApplication.class.getName());
	private static final String SEPARATEUR = "---------------------------------------------";


	// Pour éviter de pouvoir instancier la class
	private FibonacciApplication() {
	}

	public static void main(String... args) {

		Gnirps.demarre(FibonacciApplication.class);
		int val;
		Scanner clavier = new Scanner(System.in);

		LOG.info(SEPARATEUR);
		if (args.length > 0) {
			LOG.info("Liste des arguments : " + Arrays.toString(args));
			LOG.info("On prend le premier argument comme valeur de calcul : " + args[0]);
			val = Integer.valueOf(args[0]);
		}
		else {
			val = saisieClavier(clavier);
		}

		verifLimit(clavier, val);
		calcul(val);
	}

	private static void verifLimit(Scanner clavier, int val){
		final int limit = 45;
		if (val > limit){
			LOG.warning("Attention une valeur >"+limit+" demande bcp de temps pour le premier algo");
			LOG.warning("Voulez vous continuer ? (o|y)");
			if (!clavier.nextLine().matches("[OoYy]")){
				LOG.info("Sortie du programme");
				System.exit(2);
			}
		}
	}

	private static int saisieClavier(Scanner clavier){
		LOG.info("Aucun argument passé, veillez saisir un chiffre :");
		String saisie = clavier.nextLine();

		if (saisie.matches("\\d*")) {
			return Integer.valueOf(saisie);
		}else {
			LOG.info("Saisie incorrecte … sortie du programme");
			System.exit(1);
		}
		return 0;
	}

	private static void calcul(int val){

		FibonacciCalculator calculator = new Proxifier().proxify(FibonacciCalculator.class,
			new TimedInvocationHandler(new FibonacciCalculatorImpl())
		);
		FibonacciCalculator calculator2 = new Proxifier().proxify(FibonacciCalculator.class,
			new TimedInvocationHandler(new FibonacciCalculatorImpl2())
		);

		FibonacciCalculator gnirpsFibo = Gnirps.getInstance(FibonacciCalculator.class);
		FibonacciCalculator calculator3 = new Proxifier().proxify(FibonacciCalculator.class,
			new TimedInvocationHandler(gnirpsFibo)
		);

		LOG.info(SEPARATEUR);
		LOG.info(format("Calcul de la suite de Fibonacci jusqu'à %d", val));
		LOG.info(SEPARATEUR);
		String msg = val + "ème chiffre de la suite de fibonacci number : ";
		LOG.info(format("%s%d", msg, calculator.calculate(val)));
		LOG.info(SEPARATEUR);
		LOG.info(format("%s%d", msg, calculator2.calculate(val)));
		LOG.info(SEPARATEUR);
		LOG.info(format("%s%d", msg, calculator3.calculate(val)));
		LOG.info(SEPARATEUR);
	}
}
