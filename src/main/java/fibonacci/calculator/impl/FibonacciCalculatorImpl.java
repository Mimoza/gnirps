package fibonacci.calculator.impl;

import fibonacci.calculator.FibonacciCalculator;
import gnirps.annotation.Injectable;
import gnirps.annotation.Timed;

@Injectable
public class FibonacciCalculatorImpl implements FibonacciCalculator {

	@Override
	@Timed
	public long calculate(int n) {
		long result = n;
		if (n > 1) {
			result = calculate(n-1) + calculate(n-2);
		}
		return result;
	}
}
