package fibonacci.calculator.impl;

import fibonacci.calculator.FibonacciCalculator;
import gnirps.annotation.Timed;


public class FibonacciCalculatorImpl2 implements FibonacciCalculator {

    @Override
    @Timed
    public long calculate(int n) {
        long i = 0;
        long j = 1;
        for (int k = 0; k < n; k++) {
            long temp = i + j;
            i = j;
            j = temp;
        }
        return i;
    }
}
