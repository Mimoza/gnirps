package fibonacci.calculator;

public interface FibonacciCalculator {

	long calculate(int n);
}
