package gnirps;

import org.testng.annotations.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

public class ChronometerTest {

	private static class FakeClock extends Clock {
		private final long[] times;
		private int currentCall = 0;

		private FakeClock(long... times) {
			this.times = times;
		}

		@Override
		public ZoneId getZone() {
			return ZoneId.systemDefault();
		}

		@Override
		public Clock withZone(ZoneId zone) {
			return this;
		}

		@Override
		public Instant instant() {
			Instant instant = Instant.ofEpochMilli(times[currentCall]);
			currentCall = (currentCall + 1) % times.length;
			return instant;
		}
	}

	@Test
	public void should_get_elapsed_time_in_millis() {
		final long durationInMillis = 100L;
		Chronometer watch = new Chronometer(new FakeClock(0, durationInMillis));
		watch.start();

		assertThat(watch.elapsed()).isEqualTo(durationInMillis);
	}
}
