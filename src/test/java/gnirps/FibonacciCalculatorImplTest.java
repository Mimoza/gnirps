package gnirps;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import fibonacci.calculator.impl.FibonacciCalculatorImpl;

import static org.assertj.core.api.Assertions.assertThat;

public class FibonacciCalculatorImplTest {

	@DataProvider
	public static Object[][] dataFibonacci() {
		return new Object[][]{
			{ 0, 0 },
			{ 1, 1 },
			{ 2, 1 },
			{ 3, 2 },
			{ 4, 3 },
			{ 5, 5 },
			{ 6, 8 },
			{ 7, 13 },
			{ 8, 21 },
			{ 9, 34 },
			{ 10, 55 },
			{ 11, 89 },
			{ 12, 144 },
			{ 13, 233 },
			{ 14, 377 }
		};
	}

	@Test(dataProvider = "dataFibonacci")
	public void should_compute_fibonacci_number(int input, long expected) {
		assertThat(new FibonacciCalculatorImpl().calculate(input)).isEqualTo(expected);
	}
}
