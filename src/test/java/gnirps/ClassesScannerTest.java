package gnirps;

import gnirps.scanner.ClassesScanner;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

public class ClassesScannerTest {
	private static final Logger LOG = Logger.getLogger(ClassesScannerTest.class.getName());

	@Test
	public void listClassesLocal(){
		String rootPackage = this.getClass().getPackage().getName().split("\\.")[0];

		try {
			List<?> clazz = ClassesScanner.getClasses(rootPackage);
			LOG.info(clazz.toString());
			assertThat(clazz.size()).isGreaterThan(0);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}

//	@Test
//	public void list
}
