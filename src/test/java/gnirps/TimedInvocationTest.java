package gnirps;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TimedInvocationTest {

	@Test
	public void should_return_invocation_result() throws Exception {
		assertThat(new TimedInvocation().invoke(() -> 2)).isEqualTo(2);
	}
}
